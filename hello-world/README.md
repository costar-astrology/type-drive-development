This README is mean to explain what's going on in `hello.idr` because idris has no "literate-idris" variant. I'm assuming you're using spacemacs.

There are a few things demonstrated in this module:

1. How to conjure the theorem prover in spacemacs
2. How to use the proof of a lemma in a function
3. A few basic proof tactics

Open the module in a spacemacs buffer and attempt to compile using `SPC m s b`. Since there is a type hole `?plusAssocRhs` in the definition of plusAssoc, a buffer will appear prompting you to complete the type hole (click the [P] button in the buffer)

This will open two new buffers (buffers everywhere!), one to write the proof and the other to show you the hypotheses and goals as they change throughout the proof.

We want to show the `plus` is associative. You can either figure out how to do this yourself or copy and paste the spoiler below (proof valid up to alpha renaming). Use `M n` to iterate over these instructions step by step. (Note: find a way to do all at once).

```
intros
induction l
compute
trivial
intros
compute
rewrite ihn__0
trivial
```

At this point the goals buffer should read "plusAssocRhs: No more goals.", meaning we're done. You can then do `C-c C-q` to quit the proof script buffer and save the result to the `Main` module `hello.idr`.

You should then see this magically appear at the bottom of that file

```idris
---------- Proofs ----------

Main.plusAssocRhs = proof
  intros
  induction l
  compute
  trivial
  intros
  compute
  rewrite ihn__0
  trivial

```

This proof looks something like a function definition (spoiler alert, it is!). You can now recompile with `SPC m s b` and you will see that no more type holes remain and you are left with just the repl.

We then go on to define some pretty standard definition for `Vector` that shadows the one from prelude. Then we define an append function, and a double append

```idris
doubleAppend : {n,m,k : Nat} -> Vect n a -> Vect m a -> Vect k a -> Vect ((n `plus` m) `plus` k) a
doubleAppend {n} {m} {k} as bs cs =
  rewrite sym (plusAssoc n m k)
  in as `append` (bs `append` cs)

```

This syntax is weird if you're coming from ... anywhere else. The type signature is prefixed with curly braces then other stuff, as is the LHS of the definition. Those curly braces indicate _implicit_ arguments that can be used in the type language for example.

The RHS of the function/proof uses the `rewrite` directive, which allows us to take something like a proof and use it to safely coerce the type in the body. In this case we use the proof

```
sym
plusAssoc n m k
```

I'm hopelessly at a loss to explain how to know whether or not to use sym, see this [reddit thread](https://www.reddit.com/r/Idris/comments/80ufqd/proof_direction_is_backwards/) for an attempt at an explaination. 

Since the normal return type of the expression is

```idris

expr :  Vect (n `plus` (m `plus` k) a
expr = as `append` (bs `append` cs)

```

the rewrite proof allows us to reassociate the `plus` operator and gives us the safe "coercion".

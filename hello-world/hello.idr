module Main

plusAssoc : (l, c, r : Nat) -> l `plus` (c `plus` r) = (l `plus` c) `plus` r
plusAssoc = ?plusAssocRhs

data Vect : Nat -> Type -> Type where
  Nil  : Vect Z a
  (::) : a -> Vect k a -> Vect (S k) a

infixr 7 ::

append : Vect n a -> Vect m a -> Vect (n + m) a
append Nil bs = bs
append (a :: as) bs = a :: append as bs

doubleAppend : {n,m,k : Nat} -> Vect n a -> Vect m a -> Vect k a -> Vect ((n `plus` m) `plus` k) a
doubleAppend {n} {m} {k} as bs cs =
  rewrite sym (plusAssoc n m k)
  in as `append` (bs `append` cs)

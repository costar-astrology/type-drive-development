import Data.Vect

-- Reverse a vector into an accumulator
reverseAcc : Vect n elem -> Vect m elem -> Vect (n + m) elem
reverseAcc [] acc = acc
reverseAcc {n=S q} {m} (x :: xs) acc = rewrite plusSuccRightSucc q m in reverseAcc xs (x :: acc)

-- Prove that n = n+0
plusZeroIsEqual : (left : Nat) -> left = left + 0
plusZeroIsEqual left = sym $ plusZeroRightNeutral left

-- Reverse a vector using an accumulator
reverseVect : Vect n elem -> Vect n elem
reverseVect {n} xs = rewrite plusZeroIsEqual n in reverseAcc xs []

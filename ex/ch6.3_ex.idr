module Main

-- TODO: finish exercise 2

import Data.Vect

infixr 5 .+.

data Schema = SString
            | SInt
            | SChar
            | (.+.) Schema Schema

SchemaType : Schema -> Type
SchemaType SString = String
SchemaType SInt = Int
SchemaType SChar = Char
SchemaType (x .+. y) = (SchemaType x, SchemaType y)

record DataStore where
  constructor MkData
  schema : Schema
  size : Nat
  items : Vect size (SchemaType schema)

addToStore : (store : DataStore) -> SchemaType (schema store) -> DataStore
addToStore (MkData schema size store) newitem = MkData schema _ (addToData store)
  where
    addToData : Vect old (SchemaType schema) -> Vect (S old) (SchemaType schema)
    addToData [] = [newitem]
    addToData (item :: items) = item :: addToData items

data Command : Schema -> Type where
  SetSchema : (newschema : Schema) -> Command schema
  Add : SchemaType schema -> Command schema
  Get : Integer -> Command schema
  Search : String -> Command schema
  Size : Command schema
  Quit : Command schema

parseSchema : List String -> Maybe Schema
parseSchema ("String" :: []) = Just SString
parseSchema ("String" :: xs) = parseSchema xs >>= (\xs_sch => Just (SString .+. xs_sch))
parseSchema ("Int" :: []) = Just SInt
parseSchema ("Int" :: xs) = parseSchema xs >>= (\xs_sch => Just (SInt .+. xs_sch))
parseSchema ("Char" :: []) = Just SChar
parseSchema ("Char" :: xs) = parseSchema xs >>= (\xs_sch => Just (SChar .+. xs_sch))
parseSchema _ = Nothing


parsePrefix : (schema : Schema) -> String -> Maybe (SchemaType schema, String)
parsePrefix SString input = getQuoted (unpack input)
  where
    getQuoted : List Char -> Maybe (String, String)
    getQuoted ('"' :: xs)
      = case span (/= '"') xs of
             (quoted, '"' :: rest) => Just (pack quoted, ltrim (pack rest))
             _ => Nothing
    getQuoted _ = Nothing
parsePrefix SInt input = case span isDigit input of
                              ("", rest) => Nothing
                              (num, rest) => Just (cast num, ltrim rest)
parsePrefix SChar input = case unpack input of
                              (c :: []) => Just (c, "")
                              (c :: ' ' :: rest) => Just (c, pack rest)
                              _ => Nothing
parsePrefix (schemal .+. schemar) input = do
  (l_val, input') <- parsePrefix schemal input
  (r_val, input'') <- parsePrefix schemar input'
  Just ((l_val, r_val), input'')

parseBySchema : (schema : Schema) -> String -> Maybe (SchemaType schema)
parseBySchema schema input = case parsePrefix schema input of
                                  Just (res, "") => Just res
                                  Just _ => Nothing
                                  Nothing => Nothing

parseCommand : (schema : Schema) -> (cmd : String) -> (args : String) -> Maybe (Command schema)
parseCommand schema "schema" rest = do schemaok <- parseSchema (words rest)
                                       Just (SetSchema schemaok)
parseCommand schema "add" rest = do restok <- parseBySchema schema rest
                                    Just (Add restok)
parseCommand schema "get" val = case all isDigit (unpack val) of
                    False => Nothing
                    True => Just (Get (cast val))
parseCommand shema "search" str = Just $ Search str
parseCommand schema "size" "" = Just Size
parseCommand schema "quit" "" = Just Quit
parseCommand _ _ _ = Nothing

parse : (schema : Schema) -> (input : String) -> Maybe (Command schema)
parse schema input = case span (/= ' ') input of
                   (cmd, args) => parseCommand schema cmd (ltrim args)

setSchema : (store: DataStore) -> Schema -> Maybe DataStore
setSchema store schema = case size store of
                              Z => Just (MkData schema _ [])
                              S k => Nothing

display : SchemaType schema -> String
display {schema = SString} item = show item
display {schema = SInt} item = show item
display {schema = SChar} item = show item
display {schema = (x .+. y)} (iteml, itemr) = display iteml ++ ", " ++ display itemr

getEntry : (pos : Integer) -> (store : DataStore) -> (String, DataStore)
getEntry pos store =
  let store_items = items store in
        case integerToFin pos (size store) of
             Nothing => ("Out of range\n", store)
             Just id => ((display (index id store_items)) ++ "\n", store)

storeEntry : (store : DataStore) -> (item : (SchemaType (schema store))) -> (String, DataStore)
storeEntry store item = ("ID " ++ show (size store) ++ "\n", addToStore store item)

searchEntry : (substr : String) -> (store : DataStore) -> (String, DataStore)
searchEntry substr store = (displayAll (search substr store), store) where
  stringItems : (store: DataStore) -> Vect (size store) String
  stringItems store = map display $ (items store)
  indexed : Vect n String ->  List (String, Integer)
  indexed {n} vect = zip (toList vect) [0..(toIntegerNat n)]
  search : String -> DataStore -> List (String, Integer)
  search str store = filter (\(s, i) => isInfixOf str s) $ indexed $ stringItems store
  displayTuple : (String, Integer) -> String
  displayTuple (str, index) = (show index) ++ ": " ++ str
  displayAll : List (String, Integer) -> String
  displayAll strs = unlines $ map displayTuple strs

processInput : DataStore -> String -> Maybe (String, DataStore)
processInput store inp = case parse (schema store) inp of
                              Nothing => Just ("Invalid command\n", store)
                              Just (SetSchema schema') =>
                                                         case setSchema store schema' of
                                                              Nothing => Just ("Can't update schema\n", store)
                                                              Just store' => Just ("OK\n", store')
                              Just (Add item) => Just $ storeEntry store item
                              Just (Get pos) => Just $ getEntry pos store
                              Just (Search substr) => Just $ searchEntry substr store
                              Just Size => Just (show (size store) ++ "\n" , store)
                              Just Quit => Nothing

main : IO ()
main = replWith (MkData SString _ []) "Command: " processInput

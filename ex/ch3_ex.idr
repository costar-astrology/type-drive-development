module Main

import Data.Vect

--3.2
myLength : List a -> Nat
myLength [] = 0
myLength (x :: xs) = 1 + myLength xs

myReverse : List a -> List a
myReverse [] = []
myReverse (x :: xs) = myReverse xs ++ [x]

myMap : (a -> b) -> List a -> List b
myMap f [] = []
myMap f (x :: xs) = f x :: myMap f xs

myMapV : (a -> b) -> Vect n a -> Vect n b
myMapV f [] = []
myMapV f (x :: xs) = f x :: myMapV f xs


--3.3
createEmpties : Vect n (Vect 0 elem)
createEmpties {n} = replicate n []

    -- transposeHelper : (x : Vect n elem) -> (xsTrans : Vect n (Vect len elem)) -> Vect n (Vect (S len) elem)
  -- transposeHelper [] [] = []
  -- transposeHelper (x :: ys) (y :: xs) = (x :: y) :: transposeHelper ys xs

  -- transposeMat : Vect m (Vect n elem) -> Vect n (Vect m elem)
  -- transposeMat [] = createEmpties
  -- transposeMat (x :: xs) = let xsTrans = transposeMat xs
  --                         in (transposeHelper x xsTrans)

transposeMat : Vect m (Vect n elem) -> Vect n (Vect m elem)
transposeMat [] = createEmpties
transposeMat (x :: xs) = let xsTrans = transposeMat xs in
                             zipWith (::) x xsTrans

addMatrix : Num a => Vect n (Vect m a) -> Vect n (Vect m a) -> Vect n (Vect m a)
addMatrix [] [] = []
addMatrix (x :: xs) (y :: ys) = let addRest = addMatrix xs ys in
                                zipWith (+) x y :: addRest

multRow : Num a => (x : Vect m a) -> (ty : Vect m a) -> a
multRow x ty = foldl (+) 0 (zipWith (*) x ty)


multRows : Num a => (x : Vect m a) -> (tys : Vect p (Vect m a)) -> Vect p a
multRows x [] = []
multRows x (y :: ys) = multRow x y :: multRows x ys

Matrix : {n : Nat} -> {m: Nat} -> a -> Vect n (Vect m a)

multMatrix : Num a
          => Vect n (Vect m a)
          -> Vect m (Vect p a)
          -> Vect n (Vect p a)
multMatrix [] ys = []
multMatrix (x :: xs) ys = let tys = transposeMat ys in
                   (multRows x tys) :: multMatrix xs ys
 
